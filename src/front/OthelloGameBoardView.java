package front;

import javafx.scene.Parent;
import javafx.scene.layout.Pane;

public class OthelloGameBoardView extends GenericGameBoardView {

    public OthelloGameBoardView(int size){
        super(size, size);
        if (size < 4 || size % 2 != 0){
            throw new IllegalArgumentException();
        }
    }

    @Override
    public GenericBoardTile tileDefinition() {
        return new GenericBoardTile() {};
    }

    private class OthelloBoardTile extends GenericBoardTile {
        OthelloBoardTile(){

        }
    }
}
